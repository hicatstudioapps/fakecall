package badabing.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Helper class used to communicate with the server.
 * 
 * @author Junior Buckeridge A.
 */
public final class ServerUtilities {

	/**
	 * The Google project number
	 */
	private static final String META_SENDER_ID = "badabing.lib.meta.senderId";
	/**
	 * The backend public key
	 */
	private static final String META_PUBLIC_KEY = "badabing.lib.meta.publicKey";
	
	private static final String PROPERTY_GCM_REG_ID = "key.gcm.registration_id";
	private static final String PROPERTY_GCM_APP_VERSION = "key.gcm.app_version";
	private static final String PROPERTY_GCM_TIMESTAMP = "key.gcm.timestamp";
	
    private static final int MAX_ATTEMPTS = 3;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();

    public static final String KEY_DEVICE_SID = "device_sid";
    public static final String SERVER_URL = "http://push.badabingapps.com";
    private static final String API_REGISTER = "/devices/register";
    private static final String PARAM_APP_PKEY = "app_pkey";
    private static final String PARAM_SERVICE_KEY = "service_key";
    private static final String PARAM_DEVICE_TYPE = "device_type";
    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_APP_VERSION = "app_version";
    private static final String PARAM_OS_VERSION = "os_version";
	
    public static void registerWithGCM(Context context){
    	String senderID = null;
        String appPublicKey = null;
        
        try {
            ApplicationInfo app = context.getPackageManager().getApplicationInfo(context.getPackageName(),PackageManager.GET_META_DATA);
            senderID = app.metaData.getString(META_SENDER_ID); 
            appPublicKey = app.metaData.getString(META_PUBLIC_KEY); 

        } catch (Exception e) {
            e.printStackTrace();
        }
    	
        if(TextUtils.isEmpty(senderID)){
        	throw new RuntimeException("SenderID (project number) can't be NULL. Declare it in your manifest as meta with key "+META_SENDER_ID);
        }else if(TextUtils.isEmpty(appPublicKey)){
        	throw new RuntimeException("App Public Key can't be NULL. Declare it in your manifest as meta with key "+META_PUBLIC_KEY);
        }
        
    	registerGCM(context, senderID, appPublicKey);
    }
    
    private static void registerGCM(Context context, String senderID, String appPublicKey) {
		String regid = getRegistrationId(context);

        if (TextUtils.isEmpty(regid)) {
            registerInBackground(context, senderID, appPublicKey);
        }
	}
	
	private static String getRegistrationId(Context context) {
	    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
	    String registrationId = prefs.getString(PROPERTY_GCM_REG_ID, "");
	    if (TextUtils.isEmpty(registrationId)) {
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_GCM_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    long timestamp = getAppTimestamp(context);
	    if (registeredVersion != currentVersion) {
	        return "";
	    }else if(System.currentTimeMillis()-timestamp > DateUtils.WEEK_IN_MILLIS){
	        return "";
	    }
	    return registrationId;
	}
    
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	        
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	private static long getAppTimestamp(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getLong(PROPERTY_GCM_TIMESTAMP, 0);
	}

    private static void registerInBackground(final Context context, final String senderID, final String appPublicKey) {
	    new AsyncTask<Void, Void, String>() {
	        @Override
	        protected String doInBackground(Void... params) {
	            String msg = "";
	            try {
	            	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
	                
	                String regid = gcm.register(senderID);
	                msg = "Device registered, registration ID = " + regid;

	                // You should send the registration ID to your server over HTTP,
	                // so it can use GCM/HTTP or CCS to send messages to your app.
	                // The request to your server should be authenticated if your app
	                // is using accounts.
	                boolean isRegistered = register(context, regid, appPublicKey);
	                if(!isRegistered){
	                	return null;
	                }
	                
	                // Persist the regID - no need to register again.
	                storeRegistrationId(context, regid);
	                
	                // Persist timestamp
	                PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext())
	                	.edit()
	                	.putLong(PROPERTY_GCM_TIMESTAMP, System.currentTimeMillis())
	                	.commit();
	                
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	                // If there is an error, don't just keep trying to register.
	                // Require the user to click a button again, or perform
	                // exponential back-off.
	            }
	            return msg;
	        }

	    }.execute(null, null, null);
	}

    private static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_GCM_REG_ID, regId);
        editor.putInt(PROPERTY_GCM_APP_VERSION, appVersion);
        editor.commit();
    }
    
	/**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    public static boolean register(final Context context, final String regId, final String appPublicKey) {
    	String serverUrl = SERVER_URL+API_REGISTER;
        UrlEncodedFormEntity entity = null;
        ArrayList<BasicNameValuePair> parameters = new ArrayList<BasicNameValuePair>();
        parameters.add(new BasicNameValuePair(PARAM_DEVICE_TYPE, "1"));
        try {
        	parameters.add(new BasicNameValuePair(PARAM_LOCALE, Locale.getDefault().getLanguage()));
		} catch (Exception e) {
		}
        try {
        	PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        	parameters.add(new BasicNameValuePair(PARAM_APP_VERSION, "v"+info.versionName));
		} catch (Exception e) {
		}
        parameters.add(new BasicNameValuePair(PARAM_OS_VERSION, "android_"+Build.VERSION.RELEASE));
    	parameters.add(new BasicNameValuePair(PARAM_APP_PKEY, appPublicKey));
    	parameters.add(new BasicNameValuePair(PARAM_SERVICE_KEY, regId));
    	try {
        	entity = new UrlEncodedFormEntity(parameters, "UTF-8");
        	
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
        
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            try {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                JSONObject o = (JSONObject) getJSONPOST(context, serverUrl, entity, false, headers);
                String deviceSkey = o.getString(KEY_DEVICE_SID);
                if(deviceSkey != null){
                	setStringPref(context, KEY_DEVICE_SID, deviceSkey);
	                return true;
                }
                
            } catch (Exception e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
//                    Log.d("", "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    Thread.currentThread().interrupt();
                    return false;
                }

                backoff *= 2;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param context
     * @param url
     * @param entity
     * @param isJSONArray
     * @param headers
     * @return
     * @throws org.apache.http.client.ClientProtocolException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     */
    public static Object getJSONPOST(Context context, String url, HttpEntity entity, boolean isJSONArray, HashMap<String, String> headers) throws ClientProtocolException, IOException, JSONException{
		
		DefaultHttpClient client = new DefaultHttpClient();
		try{
			client.getCookieStore().clear();
		}catch(Exception e){
			e.printStackTrace();
		}
		HttpPost post = new HttpPost(url);
		post.setEntity(entity);
		if(headers != null){
			Set<String> keys = headers.keySet();
			for(String key:keys){
				post.setHeader(key, headers.get(key));
			}
		}
		HttpResponse response = client.execute(post);
		InputStream is = response.getEntity().getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				is, "UTF-8"), 8);
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n");
		}
		is.close();
		
		if(!isJSONArray)
			return new JSONObject(sb.toString());
		else
			return new JSONArray(sb.toString());
	}
    
    /**
     * Get a string preference
     * 
     * @param context
     * @param key
     * @return
     */
	public static String getStringPref(Context context, String key) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
	}

	public static void setStringPref(Context context, String key, String value) {
		
		PreferenceManager.getDefaultSharedPreferences(context).edit()
			.putString(key, value)
			.commit();
	}
}
