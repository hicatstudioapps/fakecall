package badabing.lib.utils;

import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.util.Log;

public class ImgUtils {

	// Constants
	private static final String TAG = "badabing.lib.utils.ImgUtils";
	
	public static Bitmap downloadBitmap(String url) {
		
	    final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
	    final HttpGet getRequest = new HttpGet(url);

	    try {
	        HttpResponse response = client.execute(getRequest);
	        final int statusCode = response.getStatusLine().getStatusCode();
	        if (statusCode != HttpStatus.SC_OK) { 
	        	Log.e(TAG, "Error " + statusCode + " while retrieving bitmap from " + url);
	        	return null;
	        }

	        final HttpEntity entity = response.getEntity();
	        if (entity != null) {
	            InputStream inputStream = null;
	            try {
	                inputStream = entity.getContent(); 
	                final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
	                return bitmap;
	            } finally {
	                if (inputStream != null) {
	                    inputStream.close();  
	                }
	                entity.consumeContent();
	            }
	        }
	    } catch (Exception e) {
	        getRequest.abort();
	        Log.e(TAG, "Error while retrieving bitmap from " + url, e);	        
	    } 
	    finally {
	        if (client != null) {
	            client.close();
	        }
	    }
	    return null;
	}

}
