package com.junk.call;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.widget.Toast;

@SuppressWarnings("ALL")
public class Mydatabase extends SQLiteOpenHelper {
   private static final String DATABASE_NAME = "Fakecallonex";
   private static final int DATABASE_VERSION = 1;
   public static final String IMAGE_LINK = "imglink";
   public static final String KEY_DATE = "date";
   public static final String KEY_ID = "id";
   public static final String KEY_NAME = "name";
   public static final String KEY_NUMBER = "number";
   private static final String TABLE_NAME = "fakecalltab";
   public static final String TIME = "time";
   public static final String VIB_TYPE = "vibration";
   public static final String VOICE_LINK = "voicelink";
   Context contxt;
   Cursor cr;
   SQLiteDatabase db;

   public Mydatabase(Context var1, String var2, CursorFactory var3, int var4) {
      super(var1, "Fakecallonex", (CursorFactory)null, 1);
      this.contxt = var1;
   }

   public void close() {
      this.db.close();
   }

   public void deleteAllRecord() {
      this.db = this.getWritableDatabase();
      this.db = this.getReadableDatabase();
      this.db.delete("fakecalltab", (String) null, (String[]) null);
   }

   public void deleteoneRecord(String var1) {
      this.db = this.getWritableDatabase();
      this.db = this.getReadableDatabase();
      this.db.delete("fakecalltab", var1 + "=" + "id", (String[])null);
   }

   public void insert(String var1, String var2, String var3, String var4, String var5, String var6,String var9,String var10) {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         ContentValues var7 = new ContentValues();
         var7.put("id",var9);

         var7.put("name", var1);
         var7.put("number", var2);
         var7.put("imglink", var3);
         var7.put("time", var4);
         var7.put("voicelink", var5);
         var7.put("vibration", var6);
         var7.put("ringtone",var10);
         this.db.insert("fakecalltab", (String)null, var7);
//         Toast.makeText(this.contxt, "Fake call created sucessfully.", 3000).show();
      } catch (Exception var8) {
         ;
      }

   }

   public void onCreate(SQLiteDatabase var1) {
      var1.execSQL("create table fakecalltab(id text primary key ,name text,number text UNIQUE,imglink text,time text,voicelink text,vibration text,ringtone text)");
   }

   public void onUpgrade(SQLiteDatabase var1, int var2, int var3) {
      var1.execSQL("DROP TABLE IF EXISTS fakecalltab");
      this.onCreate(var1);
   }

   public void open() {
      this.db = this.getWritableDatabase();
      this.db = this.getReadableDatabase();
   }

   public Cursor select() {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         this.cr = this.db.query("fakecalltab", (String[])null, (String)null, (String[])null, (String)null, (String)null, "id DESC");
      } catch (Exception var2) {
         this.db.close();
      }

      return this.cr;
   }

   public Cursor select(String id) {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         this.cr = this.db.query("fakecalltab", (String[])null, (String)null, new String[]{"id="+id}, (String)null, (String)null, "id DESC");

      } catch (Exception var2) {
         this.db.close();
      }

      return this.cr;
   }

   public Cursor selectImportantCalls() {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         this.cr = this.db.rawQuery("select * from callrecord where impcall =1 ORDER BY id DESC", (String[])null);
      } catch (Exception var2) {
         ;
      }

      return this.cr;
   }

   public Cursor selectIncomingCalls() {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         this.cr = this.db.rawQuery("select * from callrecord where call_type =2 ORDER BY id DESC", (String[])null);
      } catch (Exception var2) {
         ;
      }

      return this.cr;
   }

   public Cursor selectOutgoingCalls() {
      try {
         this.db = this.getWritableDatabase();
         this.db = this.getReadableDatabase();
         this.cr = this.db.rawQuery("select * from callrecord where call_type =1 ORDER BY id DESC", (String[])null);
      } catch (Exception var2) {
         ;
      }

      return this.cr;
   }

   public void updateCallrecord(String var1, String var2, String var3, String var4, String var5, String var6,String var8) {
      this.db = this.getWritableDatabase();
      this.db = this.getReadableDatabase();
      ContentValues var7 = new ContentValues();
      var7.put("name", var1);
      var7.put("number", var2);
      var7.put("imglink", var3);
      var7.put("time", var4);
      var7.put("voicelink", var5);
      var7.put("vibration", var6);
      this.db.update("fakecalltab", var7, var8 + "=" + "id", (String[])null);
   }
}
