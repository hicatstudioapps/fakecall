// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc

package com.junk.call;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.IOException;

// Referenced classes of package com.junk.call:
//            Mydatabase, Profilecustom, Listviewimage

public class Answercall extends Activity
{

    ImageView acimg;
    TextView acname;
    TextView acpno;
    Button b1;
    Button b2;
    Button b3;
    Cursor cr;
    private Handler customHandler;
    private AudioManager m_amAudioManager;
    Mydatabase mdb;
    MediaPlayer player;
    private long startTime;
    TextView time;
    long timeInMilliseconds;
    long timeSwapBuff;
    private Runnable updateTimerThread;
    long updatedTime;
    String voicepath;


    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(R.layout.answercall);
        getWindow().addFlags(1024);
        mdb = new Mydatabase(getApplicationContext(), "Fakecallonex", null, 1);
        time = (TextView)findViewById(R.id.time);
        b1 = (Button)findViewById(R.id.endcall);
        acname = (TextView)findViewById(R.id.name);
        acpno = (TextView)findViewById(R.id.pno);
        acimg = (ImageView)findViewById(R.id.cimage);
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0L);
        acname.setText(Profilecustom.cname);
        acpno.setText(Profilecustom.cpno);
        boolean flag;
        if (Profilecustom.reqcode == 3)
        {
            acimg.setImageBitmap(Profilecustom.cropbitmap);
        } else
        {

                cr = mdb.select();
                cr.moveToPosition(Listviewimage.position);
                BitmapFactory.Options bitmapFactory = new BitmapFactory.Options();
                bitmapFactory.inSampleSize = 1;
                Profilecustom.thumbnail = BitmapFactory.decodeFile(Profilecustom.lpath, bitmapFactory);
                acimg.setImageBitmap(Profilecustom.thumbnail);


        }
        flag = Profilecustom.reccb.isChecked();
        if (flag)
        {

            try {
                playvoice();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        b1.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {

                if (player.isPlaying()) {
                    player.stop();
                    player.release();
                    player = null;
                    finish();
                }

                // Misplaced declaration of an exception variable
            }});
    }

    public void onDestroy()
    {
        super.onDestroy();
        if(player!=null){
            if(player.isPlaying())
                player.stop();
            player.release();
            player = null;
        }

    }

    public void playvoice()
            throws IllegalArgumentException, SecurityException, IllegalStateException, IOException
    {
        cr = mdb.select();
        cr.moveToPosition(Listviewimage.position);
        voicepath = cr.getString(5);
        player = new MediaPlayer();
        player.setDataSource(voicepath);
        player.prepare();
        m_amAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        m_amAudioManager.setMode(2);
        m_amAudioManager.setSpeakerphoneOn(false);
        player.start();
    }



}
