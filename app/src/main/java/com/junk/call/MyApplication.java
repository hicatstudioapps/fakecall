package com.junk.call;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.junk.call.json.ImageLoader;

public class MyApplication extends Application {

   public static int position;
   public static InterstitialAd interstitialAd;
   AdRequest request;
   public static Typeface face;
   public static String LAST_PROFILE="lasP";
   public static String NAME="name";
   public static String NUMBER="num";
   public static String IMAGE_PATH="IMAGE";
   public static String DELAY="DELAY";
   public static String VOICE="VOICE";
   public static String VIBRATE="VIBRATE";
   public static String ID="ID";
   public static String RINGTONE="RT";
   public static Typeface RL;
   public static Typeface RM;
   //public static SharedPreferences preferences;
   public static Context context;



   public static void setPosition(int position) {
      MyApplication.position = position;
   }

   public void onCreate() {
   super.onCreate();
      context=this;
//      SharedPreferences sp=PrefUtils.prefs(this);
//      if(sp.getBoolean("firtsT",true)){
//         sp.edit().putBoolean("blue",true).commit();
//         sp.edit().putBoolean("firstT",false).commit();
//      }
      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
      com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId("ca-app-pub-1147824671772507/4145900872");
      request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
         @Override
         public void onAdClosed() {
            interstitialAd.loadAd(request);
         }
      });
      interstitialAd.loadAd(request);
//        if(getSharedPreferences("data",MODE_PRIVATE).getBoolean(Constants.CONTENT_READY,false))
      RL= Typeface.createFromAsset(getAssets(),"Roboto-Light.ttf");
      RM= Typeface.createFromAsset(getAssets(),"Roboto-Medium.ttf");

   }

   public static void showInterstitial(){
      if(interstitialAd!= null && interstitialAd.isLoaded()){
         interstitialAd.show();
      }
   }

   public static void putProfile(Context context, String name, String number, String image, String delayPosition,String voicePath, String vibrate,String id,String d,String r){
      SharedPreferences.Editor editor= context.getSharedPreferences(LAST_PROFILE, MODE_PRIVATE).edit();
      editor.putString(NAME,name);
      editor.putString(NUMBER,number);
      editor.putString(IMAGE_PATH,image);
      editor.putString(DELAY,delayPosition);
      editor.putString("L",d);
      editor.putString(VOICE,voicePath);
      editor.putString(VIBRATE,vibrate);
      editor.putString(ID,id);
      editor.putString(RINGTONE,r);
      editor.commit();
   }

   public static String [] getProfile(Context context){
      SharedPreferences editor= context.getSharedPreferences(LAST_PROFILE, MODE_PRIVATE);
      String [] result= new String[9];
      result[0]=editor.getString(NAME,"");
      result[1]= editor.getString(NUMBER,"");
      result[2]= editor.getString(IMAGE_PATH,"");
      result[3]=editor.getString(DELAY,"")+"";
      result[4]= editor.getString(VOICE,"");
      result[5]= editor.getString(VIBRATE,"");
      result[6]= editor.getString(ID,"");
      result[7]=editor.getString("L","");
      result[8]=editor.getString(RINGTONE,"");
      return result;
   }

}
