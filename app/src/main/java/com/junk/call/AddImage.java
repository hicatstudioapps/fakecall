package com.junk.call;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.NetworkInfo.State;
import android.net.rtp.AudioStream;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.junk.call.CustomSms;
import com.junk.call.Listviewimage;
import com.junk.call.Profilecustom;
import com.junk.call.json.ImageLoader;
import com.junk.call.other.Constants;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import badabing.lib.ServerUtilities;
import badabing.lib.Utils;
import badabing.lib.apprater.AppRater;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

@SuppressWarnings("ResourceType")
public class AddImage extends AppCompatActivity {

    static String COUNTRY = "appimage";
    static String FLAG = "appimage";
    static String POPULATION = "apppackage";
    static String RANK = "appname";
    static ArrayList arraylist;
    public static int count = 0;
    static ImageLoader imageLoader;
    static Boolean isInternetPresent;


    AdView adView;
    Button add2;
    Button addinHorizontalScrollView;
    ImageView app1;
    ImageView app2;
    ImageView app3;
    SharedPreferences app_Preferences1;
    SharedPreferences app_Preferences2;
    Button cussms;
    private InterstitialAd interstitial;
    Button moreApps;
    private int posi;
    @InjectView(R.id.profile_image)
    CircleImageView profile;
    @InjectView(R.id.ctime)
    TextView time;
    @InjectView(R.id.cname)
    TextView cname;
    @InjectView(R.id.profiles)
    View edit;
    @InjectView(R.id.edit)
            View editB;
    @InjectView(R.id.call)
            Button call;
    View root;
    String[] data;

    private ContextMenuDialogFragment mMenuDialogFragment;
    private MediaPlayer player1;
    private MediaPlayer.OnCompletionListener onCompletionListener;
    private AudioManager m_amAudioManager;
    private boolean playVoice;
    private boolean iswindow;
    private MediaPlayer player;
    boolean vib;
    Vibrator vbr;
    private final Runnable mRunnable = new Runnable() {

        public void run() {
            try {
                setwindow();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };
    long startTime = 0L;
    long timeSwapBuff = 0L;
    private Long timeInMilliseconds, updatedTime;

    Runnable updateTimerThread2 = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;

            int i = (int) (updatedTime / 1000L);
            int j = i / 60;
            time1.setText((new StringBuilder("0")).append(j).append(":").append(String.format("%02d", new Object[]{
                    Integer.valueOf(i % 60)

            })).toString());
            customHandler.postDelayed(this, 0L);
        }

    };
    private Handler customHandler = new Handler();
    private TextView time1;

    private void showDialog() {
//      Builder var1 = new Builder(this);
//      var1.setTitle("Rate App");
//      var1.setMessage("Love this App..?\nPlease rate it 5 stars.");
//      var1.setPositiveButton("YES", new OnClickListener() {
//         public void onClick(DialogInterface var1, int var2) {
//            Editor var4 = AddImage.this.app_Preferences1.edit();
//            var4.putInt("posi", 30);
//            var4.commit();
//
//            try {
//               Intent var5 = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%1$s", new Object[]{AddImage.this.getPackageName()})));
//               AddImage.this.startActivity(var5);
//            } catch (Exception var3) {
//               ;
//            }
//
//         }
//      });
//      var1.setNegativeButton("NO", new OnClickListener() {
//         public void onClick(DialogInterface var1, int var2) {
//            var1.cancel();
//         }
//      });
//      var1.show();
    }

    @OnClick({R.id.edit, R.id.rate, R.id.share, R.id.more, R.id.call})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.edit:
                String[] data = MyApplication.getProfile(this);
                if (data[6].isEmpty()) {
                    Toast.makeText(this, getString(R.string.home_error), Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(this, Profilecustom.class);
//         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    //nombre
                    bundle.putBoolean("list", true);
                    bundle.putString(Constants.name, data[0]);
                    bundle.putString(Constants.phone, data[1]);
                    bundle.putString(Constants.img, data[2]);
                    bundle.putString(Constants.id, data[6]);
                    bundle.putString(Constants.vibrate, data[5]);
                    bundle.putString(Constants.voice, data[4]);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            case R.id.rate:
                AppRater.rateNow(this);
                break;
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String text = String.format(getString(R.string.share_app_default_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id=" + getPackageName());
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.call:
                data = MyApplication.getProfile(this);
                if (data[6].isEmpty()) {
                    Toast.makeText(this, getString(R.string.home_error), Toast.LENGTH_LONG).show();
                } else {
                    new Handler().postDelayed(mRunnable, Long.parseLong(data[7]));
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.HOME");
                    intent.setFlags(0x10000000);
                    startActivity(intent);
                    Toast.makeText(this, getString(R.string.call_message) +" "+ data[3], Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.more:
                startActivity(new Intent(this, OutrasAppsActivity.class));
                break;
        }


    }


    public boolean isConnectingToInternet() {
        ConnectivityManager var1 = (ConnectivityManager) this.getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
        boolean var3;
        if (var1 != null) {
            NetworkInfo[] var4 = var1.getAllNetworkInfo();
            if (var4 != null) {
                for (int var2 = 0; var2 < var4.length; ++var2) {
                    if (var4[var2].getState() == State.CONNECTED) {
                        var3 = true;
                        return var3;
                    }
                }
            }
        }

        var3 = false;
        return var3;
    }

    public void onBackPressed() {
        //  this.showAppsDialog();
        MyApplication.showInterstitial();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        data = MyApplication.getProfile(this);
        if (!data[6].isEmpty()) {

//            ShowcaseConfig config = new ShowcaseConfig();
//            config.setContentTextColor(getResources().getColor(R.color.green));
//            config.setMaskColor(getResources().getColor(R.color.tutorial_color));
//            config.setDelay(200); // half second between each showcase view
//
//            MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this,"555");
//
//            sequence.setConfig(config);
//
//            sequence.addSequenceItem(editB,
//                    getString(R.string.home_tutorial_edit), getString(R.string.home_tutorial_ok));
//
//            sequence.addSequenceItem(call,getString(R.string.home_tutorial_call),
//                    getString(R.string.home_tutorial_ok));
//            sequence.start();
            cname.setText(data[0]);
            if (!data[2].isEmpty())
                profile.setImageURI(Uri.parse(data[2]));
            else
                profile.setImageResource(R.drawable.avatar);
            time.setText(data[3]);
        }
    }

    public void onCreate(Bundle var1) {
        super.onCreate(var1);
        this.setContentView(R.layout.addimage);
        ButterKnife.inject(this);
        //fuente
        time.setTypeface(MyApplication.RL);
        cname.setTypeface(MyApplication.RL);
        ServerUtilities.registerWithGCM(this);
        //Nuevo menu
        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);

        MenuObject send = new MenuObject("Send message");
        send.setResource(R.drawable.icn_1);

        MenuObject send2 = new MenuObject("Send message");
        send2.setResource(R.drawable.icn_2);

        MenuObject send3 = new MenuObject("Send message");
        send3.setResource(R.drawable.icn_3);

        MenuObject send4 = new MenuObject("Send message");
        send3.setResource(R.drawable.icn_4);

        List<MenuObject> menuObjects = new ArrayList<>();
        menuObjects.add(close);
        menuObjects.add(send);
        menuObjects.add(send2);
        menuObjects.add(send3);
        menuObjects.add(send4);

        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.abc_action_bar_default_height_material));
        menuParams.setMenuObjects(menuObjects);
        menuParams.setClosableOutside(true);
        // set other settings to meet your needs
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
//
//      try {
//         this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3ec587")));
//      } catch (Exception var10) {
//         ;
//      }

        this.add2 = (Button) this.findViewById(R.id.profiles);
        add2.setTypeface(MyApplication.RM);
        call.setTypeface(MyApplication.RM);
        this.addinHorizontalScrollView = (Button) this.findViewById(R.id.addinhorizontalscrollview);
        this.cussms = (Button) this.findViewById(R.id.customsms);
        this.moreApps = (Button) this.findViewById(R.id.moreapps);
        this.app1 = (ImageView) this.findViewById(R.id.app1);
        this.app2 = (ImageView) this.findViewById(R.id.app2);
        this.app3 = (ImageView) this.findViewById(R.id.app3);
        Animation var12 = AnimationUtils.loadAnimation(this, R.anim.zoomin);
        this.app1.setAnimation(var12);
        this.app2.setAnimation(var12);
        this.app3.setAnimation(var12);

        try {
            isInternetPresent = Boolean.valueOf(this.isConnectingToInternet());
        } catch (Exception var9) {
            ;
        }

        AdRequest var13;
        try {
            this.adView = (AdView) this.findViewById(R.id.adView);
            var13 = (new AdRequest.Builder()).build();
            this.adView.loadAd(var13);
            this.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception var8) {
            ;
        }

        try {
            View var14 = this.findViewById(R.id.top);
            if (!isInternetPresent.booleanValue()) {
                //noinspection ResourceType
                var14.setVisibility(8);
            }
        } catch (Exception var7) {
            ;
        }

        try {
            this.interstitial = new InterstitialAd(this);
            this.interstitial.setAdUnitId("ca-app-pub-8098707670633703/5411584071");
            var13 = (new AdRequest.Builder()).build();
            this.interstitial.loadAd(var13);
        } catch (Exception var6) {
            ;
        }


        this.addinHorizontalScrollView.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View var1) {
                Intent var2 = new Intent(AddImage.this, Profilecustom.class);
                startActivity(var2);
            }
        });
        this.add2.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View var1) {
                Intent var2 = new Intent(AddImage.this, Listviewimage.class);
                AddImage.this.startActivity(var2);
            }
        });
        this.cussms.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View var1) {
                Intent var2 = new Intent(AddImage.this, CustomSms.class);
                AddImage.this.startActivity(var2);
            }
        });
        this.moreApps.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View var1) {
                Intent var2 = new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://search?q=pub:Onex+Softech", new Object[0])));
                AddImage.this.startActivity(var2);
            }
        });
        this.app2.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View param1) {
                // $FF: Couldn't be decompiled
            }
        });
        this.app1.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View param1) {
                // $FF: Couldn't be decompiled
            }
        });
        this.app3.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View param1) {
                // $FF: Couldn't be decompiled
            }
        });

        try {
            this.app_Preferences1 = PreferenceManager.getDefaultSharedPreferences(this);
            this.app_Preferences2 = PreferenceManager.getDefaultSharedPreferences(this);
            this.posi = this.app_Preferences1.getInt("posi", 0);
            Editor var15 = this.app_Preferences1.edit();
            var15.putInt("posi", this.posi + 1);
            var15.commit();
        } catch (Exception var5) {
            ;
        }

        try {
            this.app_Preferences1 = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
            if (this.posi >= 1 && this.posi <= 25) {
                this.showDialog();
            }
        } catch (Exception var4) {
            ;
        }

//        new MaterialShowcaseView.Builder(this)
//                .setTarget(edit)
//                .setDismissText(getString(R.string.home_tutorial_ok))
//                .setContentText(getString(R.string.home_tutorial))
//                .setContentTextColor(getResources().getColor(R.color.green))
//                .setMaskColour(getResources().getColor(R.color.tutorial_color))
//                .setDelay(1000) // optional but starting animations immediately in onCreate can make them choppy
//                .singleUse("666") // provide a unique ID used to ensure it is only shown once
//                .show();

    }

    public boolean onCreateOptionsMenu(Menu var1) {
        this.getMenuInflater().inflate(R.menu.menushare, var1);
        var1.findItem(R.id.menu_share);
        return true;
    }


    public void playvoice() throws IOException {

        player1 = new MediaPlayer();
        player1.setDataSource(MyApplication.getProfile(this)[4]);
        player1.setOnCompletionListener(onCompletionListener);
        m_amAudioManager = (AudioManager) getSystemService("audio");
        m_amAudioManager.setMode(2);
        m_amAudioManager.setSpeakerphoneOn(false);
        player1.setVolume(1.0F, 1.0F);
        player1.prepare();
        player1.start();
    }

    public void setwindow() throws IOException {
        onCompletionListener = new android.media.MediaPlayer.OnCompletionListener() {


            public void onCompletion(MediaPlayer mediaplayer) {

                if (player != null) {

                    player.release();
                    //player = null;
                }

            }
        };
        data = MyApplication.getProfile(this);
        final View mOverlay;
        final WindowManager mWindowManager;
        android.view.WindowManager.LayoutParams layoutparams;
        layoutparams = new android.view.WindowManager.LayoutParams(-1, -1, 2007, 40, -3);
        getWindow().addFlags(1024);
        layoutparams.gravity = 48;
        mOverlay = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.main, null);
        mWindowManager = (WindowManager) getApplicationContext().getSystemService("window");
        ImageView cimage = (ImageView) mOverlay.findViewById(R.id.cimage);
        TextView sname = (TextView) mOverlay.findViewById(R.id.name);
        TextView spno = (TextView) mOverlay.findViewById(R.id.pno);
        long al[];
        ImageButton reject = (ImageButton) mOverlay.findViewById(R.id.stop);
        ImageButton answer = (ImageButton) mOverlay.findViewById(R.id.answer);

        sname.setText(data[0]);
        spno.setText(data[1]);

        String lpath = data[2];
        if (!lpath.isEmpty())
            cimage.setImageURI(Uri.parse(lpath));
        else
            cimage.setImageResource(R.drawable.avatar);
        if (data[4].equals("n"))
            playVoice = true;
        else playVoice = false;

//        }
        mWindowManager.addView(mOverlay, layoutparams);
        iswindow = true;

        if (data[5].equals("y")) {
            vib = true;
        } else
            vib = false;

        al = new long[3];
        al[1] = 700L;
        al[2] = 1500L;
        vbr = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vbr.vibrate(al, 0);
        player = new MediaPlayer();
        player.setAudioStreamType(2);
        try {
            if (data[8].isEmpty())
                player.setDataSource(this, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
            else
                player.setDataSource(this, Uri.parse(data[8]));
            player.setVolume(1.0F, 1.0F);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
            player.setDataSource(this, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
            player.setVolume(1.0F, 1.0F);
            player.prepare();
            player.start();
        }

//        _L4:
        reject.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                if (vib) {
                    vbr.cancel();
                }
                if (player.isPlaying()) {
                    player.stop();
                }
                player.release();
//            player = null;
                mWindowManager.removeView(mOverlay);
            }

        });
        answer.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                vbr.cancel();
                if (player.isPlaying()) {
                    player.stop();
                }
                player.release();
                //  player = null;
                mWindowManager.removeView(mOverlay);
                answerWindow();
            }

        });
    }

    public void answerWindow() {
        data = MyApplication.getProfile(this);
        android.view.WindowManager.LayoutParams layoutparams;
        final View ansview;
        final WindowManager mWindowManager;
        ImageView obj;
        Object obj2;
        layoutparams = new android.view.WindowManager.LayoutParams(-1, -1, 2007, 40, -3);
        getWindow().addFlags(1024);
        layoutparams.gravity = 48;
        ansview = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.answercall, null);
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE);
        obj2 = new Mydatabase(getApplicationContext(), "Fakecallonex", null, 1);
        time1 = (TextView) ansview.findViewById(R.id.time);
        ImageButton button = (ImageButton) ansview.findViewById(R.id.endcall);
        TextView acname = (TextView) ansview.findViewById(R.id.name);
        TextView acpno = (TextView) ansview.findViewById(R.id.pno);
        obj = (ImageView) ansview.findViewById(R.id.cimage);
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread2, 0L);
        acname.setText(data[0]);
        acpno.setText(data[1]);
        String lpath = data[2];
        if (!lpath.isEmpty())
            obj.setImageURI(Uri.parse(lpath));
        else
            obj.setImageResource(R.drawable.avatar);
        if (!data[4].isEmpty())
            try {
                playvoice();
            } catch (IOException e) {
                e.printStackTrace();
            }
        mWindowManager.addView(ansview, layoutparams);
//
        button.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {

                if (player1 != null && player1.isPlaying()) {
                    player1.stop();
                    player1.reset();
                    player1.release();
                }

                ///player1 = null;
                customHandler.removeCallbacks(updateTimerThread2);
                mWindowManager.removeView(ansview);
                //finish();
                //MyApplication.showInterstitial();
            }
        });
//            return;


    }


}
