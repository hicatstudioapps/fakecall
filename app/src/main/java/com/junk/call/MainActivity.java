package com.junk.call;

import android.app.Activity;
import android.app.NotificationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.junk.call.CustomSms;
import com.junk.call.MessagesListAdapter;
import com.junk.call.other.Message;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends Activity {

   static String Msg1;
   static String Msg2;
   static String Msg3;

   private MessagesListAdapter adapter;
   private ImageView btnSend;
   long delaytime = 4000L;
   private EditText inputMsg;
   private InterstitialAd interstitial;
   int j = 0;
   private List listMessages;
   private ListView listViewMessages;
   String lname;
   Message m;
   private Handler mHander = new Handler();

   private final Runnable mRunnable = new Runnable() {
      public void run() {

         MainActivity var1 = MainActivity.this;
         ++var1.j;

         try {
            if(MainActivity.this.j != 0 && MainActivity.this.j <= CustomSms.listMessages.size()) {
               MainActivity.this.m = (Message)CustomSms.listMessages.get(MainActivity.this.j);
               MainActivity.this.msg = MainActivity.this.m.getMessage();
               MainActivity.this.lname = CustomSms.msgname;
               MainActivity.this.m = new Message(MainActivity.this.lname, MainActivity.this.msg, (new SimpleDateFormat("hh:mm a")).format(new Date()), false);
               MainActivity.this.appendMessage(MainActivity.this.m);
               MainActivity.this.playBeep();
            } else {
               Toast.makeText(MainActivity.this.getApplicationContext(), "No Masgs..", 2000).show();
            }
         } catch (Exception var3) {
            ;
         }

         MainActivity.this.mHander.postDelayed(MainActivity.this.mRunnable, MainActivity.this.delaytime);

         try {
            MainActivity.this.mHander.removeCallbacks(MainActivity.this.mRunnable);
         } catch (Exception var2) {
            ;
         }

      }
   };
   String msg;
   private String name = null;
   TextView pname;
   TextView pnum;
   ImageView pview;

   private void appendMessage(final Message var1) {
      this.runOnUiThread(new Runnable() {
         public void run() {
            MainActivity.this.listMessages.add(var1);
            MainActivity.this.adapter.notifyDataSetChanged();

            try {
               MainActivity.this.mHander.removeCallbacks(MainActivity.this.mRunnable);
            } catch (Exception var2) {
               ;
            }

         }
      });
   }

   public void displayInterstitial() {
      if(this.interstitial.isLoaded()) {
         this.interstitial.show();
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.requestWindowFeature(1);
      this.setContentView(R.layout.activity_main);
      this.btnSend = (ImageView)this.findViewById(R.id.btnSend);
      this.inputMsg = (EditText)this.findViewById(R.id.inputMsg);
      this.listViewMessages = (ListView)this.findViewById(R.id.list_view_messages);
      this.pview = (ImageView)this.findViewById(R.id.pimg);
      this.pname = (TextView)this.findViewById(R.id.pname);
      this.pnum = (TextView)this.findViewById(R.id.pnum);

      try {
         this.interstitial = new InterstitialAd(this);
         this.interstitial.setAdUnitId("ca-app-pub-8098707670633703/5411584071");
         AdRequest var6 = (new AdRequest.Builder()).build();
         this.interstitial.loadAd(var6);
      } catch (Exception var5) {
         ;
      }

      this.interstitial.setAdListener(new AdListener() {
         public void onAdLoaded() {
            MainActivity.this.displayInterstitial();
         }
      });
      if(CustomSms.reqcode == 3) {
         this.pview.setImageBitmap(CustomSms.cropbitmap);
      } else if(CustomSms.reqcode == 5) {
         try {
            this.pview.setImageURI(Uri.parse(CustomSms.image_uri));
         } catch (Exception var4) {
            ;
         }
      } else {
         this.pview.setImageResource(R.drawable.amsg);
      }

      try {
         this.pname.setText(CustomSms.name.getText().toString().trim());
         this.pnum.setText(CustomSms.number.getText().toString().trim());
      } catch (Exception var3) {
         ;
      }

      ((NotificationManager)this.getSystemService("notification")).cancel(0);
      this.name = this.getIntent().getStringExtra("name");
      this.btnSend.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            MainActivity.this.m = new Message("You", MainActivity.this.inputMsg.getText().toString().trim(), (new SimpleDateFormat("hh:mm a")).format(new Date()), true);
            MainActivity.this.appendMessage(MainActivity.this.m);
            MainActivity.this.inputMsg.setText((CharSequence)null);
            MainActivity.this.mHander.postDelayed(MainActivity.this.mRunnable, MainActivity.this.delaytime);
         }
      });
      this.listMessages = new ArrayList();
      this.adapter = new MessagesListAdapter(this, this.listMessages);
      this.listViewMessages.setAdapter(this.adapter);

      try {
         this.m = (Message)CustomSms.listMessages.get(this.j);
         this.msg = this.m.getMessage();
         this.lname = this.m.getFromName();
         this.m = new Message(this.lname, this.msg, (new SimpleDateFormat("hh:mm a")).format(new Date()), false);
         this.appendMessage(this.m);
      } catch (Exception var2) {
         ;
      }

   }

   public void playBeep() {
      try {
         Uri var1 = RingtoneManager.getDefaultUri(2);
         RingtoneManager.getRingtone(this.getApplicationContext(), var1).play();
      } catch (Exception var2) {
         var2.printStackTrace();
      }

   }
}
