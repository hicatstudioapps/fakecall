package com.junk.call;

import android.net.Uri;

/**
 * Created by La Costra on 01/09/2015.
 */
public class FakeRecord{

    String ringtone;
    String delay;
    Uri imagePath;
    String cname;
    String cnum;
    String vbr;
    String rec;
    String id;
    String time;

    public FakeRecord(Uri imagePath, String cname, String cnum, String vbr, String rec, String id, String time, String delay, String ringtone) {
        this.imagePath = imagePath;
        this.cname = cname;
        this.cnum = cnum;
        this.vbr = vbr;
        this.rec = rec;
        this.id = id;
        this.time=time;
        this.delay=delay;
        this.ringtone=ringtone;
    }

    public FakeRecord(Uri imagePath, String cname, String cnum) {
        this.imagePath = imagePath;
        this.cname = cname;
        this.cnum = cnum;
    }}
