// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc

package com.junk.call;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("ResourceType")
public class Recordvoice
        implements android.media.MediaPlayer.OnErrorListener {

    public static final String DEFAULT_STORAGE_LOCATION = "/sdcard/FakeCallonex/Audio";
    static boolean islistrecord = false;

    public static File getRecording() {
        return recording;
    }

    public static File recording = null;

    public static String getRecordsrc() {
        return recordsrc;
    }

    public static String recordsrc;
    AdView adView;
    Calendar c;
    String date;
    String datetime;
    SimpleDateFormat df;
    private InterstitialAd interstitial;
    MediaPlayer m;
    private MediaRecorder myAudioRecorder;
    private android.media.MediaPlayer.OnCompletionListener onCompletionListener;
    private Button play;
    String prefix;
    private Button start;
    private Button stop;
    String suffix;
    String time;

    private File makeOutputFile() {
        File file = new File("/sdcard/FakeCallonex/Audio");
        Object obj;
        if (!file.exists()) {
            try {
                file.mkdirs();
            } catch (Exception exception) {
                Log.e("CallRecorder", (new StringBuilder("RecordService::makeOutputFile unable to create directory ")).append(file).append(": ").append(exception).toString());
                return null;
            }
        } else if (!file.canWrite()) {
            return null;
        }
        obj = datetime.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
        prefix = (new StringBuilder("callrecord")).append(((String) (obj))).toString();
        suffix = ".3gp";
        try {
            obj = File.createTempFile(prefix, suffix, file);
        } catch (IOException ioexception) {
            Log.e("CallRecorder", (new StringBuilder("RecordService::makeOutputFile unable to create temp file in ")).append(file).append(": ").append(ioexception).toString());
            return null;
        }
        return ((File) (obj));
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }



    public void onCreate() {
      onCompletionListener = new android.media.MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mediaplayer) {
                m.release();
                m = null;
                play.setVisibility(0);
                stop.setVisibility(8);
                start.setVisibility(8);
            }


        };
        c = Calendar.getInstance();
        df = new SimpleDateFormat("dd-MM-yyyy");
        date = df.format(c.getTime());
        time = DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
        datetime = (new StringBuilder(String.valueOf(date))).append(" ").append(time).toString();
        String replace = datetime.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
        prefix = (new StringBuilder("callrecord")).append(replace).toString();
        suffix = ".3gp";
        recording = makeOutputFile();
        recordsrc="";
        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(1);
        myAudioRecorder.setOutputFormat(1);
        myAudioRecorder.setAudioEncoder(3);
        myAudioRecorder.setOutputFile(recording.getAbsolutePath());

    }

    public Recordvoice() {
        c = Calendar.getInstance();
        df = new SimpleDateFormat("dd-MM-yyyy");
        date = df.format(c.getTime());
        time = DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
        datetime = (new StringBuilder(String.valueOf(date))).append(" ").append(time).toString();
        recording = makeOutputFile();
        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(1);
        myAudioRecorder.setOutputFormat(1);
        myAudioRecorder.setAudioEncoder(3);
        myAudioRecorder.setOutputFile(recording.getAbsolutePath());
    }

    public boolean onError(MediaPlayer mediaplayer, int i, int j) {

        if (myAudioRecorder != null) {
            myAudioRecorder.reset();
            myAudioRecorder.stop();
            myAudioRecorder.release();
            myAudioRecorder = null;
        }
        return false;

    }




    public void stop( ) {

       // islistrecord = true;
        myAudioRecorder.stop();
        myAudioRecorder.release();
        //myAudioRecorder = null;
        recordsrc = recording.getAbsolutePath();
        // Misplaced declaration of an exception variable

       // Toast.makeText(getApplicationContext(), "Audio recorded successfully", 1).show();

    }

    public void startrec() {
        try {
            myAudioRecorder.prepare();
            myAudioRecorder.start();
        } catch (IllegalStateException illegalstateexception) {
            illegalstateexception.printStackTrace();
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(), "Recording started", 1).show();
    }

}
