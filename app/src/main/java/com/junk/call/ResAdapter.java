package com.junk.call;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.junk.call.other.Message;
import java.util.List;

public class ResAdapter extends BaseAdapter {
   static List messagesItems;
   private Context context;

   public ResAdapter(Context var1, List var2) {
      this.context = var1;
      messagesItems = var2;
   }

   public int getCount() {
      return messagesItems.size();
   }

   public Object getItem(int var1) {
      return messagesItems.get(var1);
   }

   public long getItemId(int var1) {
      return (long)var1;
   }

   @SuppressLint({"InflateParams"})
   public View getView(int var1, View var2, ViewGroup var3) {
      Message var5 = (Message)messagesItems.get(var1);
      var2 = ((LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.addmsgitem, (ViewGroup)null);
      TextView var4 = (TextView)var2.findViewById(R.id.lblMsgFrom);
      ((TextView)var2.findViewById(R.id.txtMsg)).setText(var5.getMessage());
      var4.setText(var5.getFromName());
      return var2;
   }
}
