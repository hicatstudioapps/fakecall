// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc

package com.junk.call;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.junk.call.other.Constants;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

// Referenced classes of package com.junk.call:
//            Mydatabase, Listviewimage, AddImage, Recordvoice,
//            Params, Clickrec

@SuppressWarnings("ResourceType")
public class Profilecustom extends AppCompatActivity
        implements android.media.MediaPlayer.OnErrorListener {

    static int PICK_CONTACT_REQUEST = 5;
    static Bitmap bitmap;
    static String cname;
    static String cpno;
    static Bitmap cropbitmap;
    static EditText ename;
    static EditText ephoneno;
    static String image_uri;
    static String lpath;
    static CheckBox reccb;
    static int reqcode;
    static Bitmap thumbnail;
    static CheckBox vibcb;
    final int PIC_CROP = 3;
    private final Runnable updateTimerThread2;
    private final Handler mHander;
    private final Runnable updateTimerThread;

    Button b;
    File camfile;

    final Handler customHandler = new Handler();
    String dbimgpath;
    long delaytime;
    android.content.SharedPreferences.Editor editor;
    boolean iswindow;
    Mydatabase mdb;
    Button parambtn;
    RelativeLayout paramrl;
    Button params;
    int parmpos;
    String path;
    Button pickcontact;

    MediaPlayer player;
    MediaPlayer player1;
    SharedPreferences pref;
    Button recb;
    RelativeLayout recordrl;

    Spinner spinner1;

    long startTime;
    TextView time;
    long timeInMilliseconds;
    long timeSwapBuff;
    long updatedTime;
    Vibrator vbr;
    boolean vib;
    RelativeLayout vibraterl;
    ImageView viewImage;
    String voicepath = "";
    Context context;
    boolean recording = false;
    String ringtonePath="";
    TextView dialogTime;
    boolean fromList = false;
    @InjectView(R.id.editText)
    EditText ringtoneEt;
    Recordvoice record;
    String id;

    public Profilecustom() {
        iswindow = false;
        vib = false;
        mHander = new Handler();
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedTime = 0L;
        startTime = 0L;
        dbimgpath = "";
        updateTimerThread = new Runnable() {

            public void run() {
                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;
                int i = (int) (updatedTime / 1000L);
                int j = i / 60;
                time.setText((new StringBuilder("0")).append(j).append(":").append(String.format("%02d", new Object[]{
                        Integer.valueOf(i % 60)
                })).toString());
                customHandler.postDelayed(this, 0L);
            }

        };
        updateTimerThread2 = new Runnable() {

            public void run() {
                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;

                int i = (int) (updatedTime / 1000L);
                int j = i / 60;
                dialogTime.setText((new StringBuilder("0")).append(j).append(":").append(String.format("%02d", new Object[]{
                        Integer.valueOf(i % 60)

                })).toString());
                customHandler.postDelayed(this, 0L);
            }

        };

    }

    private void performCrop(Uri uri) {

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 128);
        intent.putExtra("outputY", 128);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 3);

    }

    private void selectImage() {
        final CharSequence options[] = new CharSequence[3];
        options[0] = getResources().getString(R.string.profile_take_photo);
        options[1] = getResources().getString(R.string.profile_chose_image);
        options[2] = getResources().getString(R.string.profile_chose_contact);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.profile_dialog_title));
        builder.setItems(options, new android.content.DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialoginterface, int i) {
                Intent intent;
                if (options[i].equals(getResources().getString(R.string.profile_take_photo))) {
                    intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    camfile = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra("output", Uri.fromFile(camfile));
                    startActivityForResult(intent, 1);
                } else {
                    if (options[i].equals(getResources().getString(R.string.profile_chose_image))) {
                        intent = new Intent("android.intent.action.PICK", android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 2);
                        return;
                    }
                    if (options[i].equals(getResources().getString(R.string.profile_chose_contact))) {
                        intent = new Intent("android.intent.action.PICK", android.provider.ContactsContract.Contacts.CONTENT_URI);
                        intent.setType("vnd.android.cursor.dir/phone_v2");
                        startActivityForResult(intent, 5);
                        return;
                    }
                }
            }
        });
        builder.show();
    }



    public void makeDirectory() {
        File file = new File("/sdcard/FakeCallonex");
        boolean flag = true;
        if (!file.exists()) {
            flag = file.mkdir();
        }
        if (flag) {
            (new File("/sdcard/FakeCall/Picture")).mkdir();
            (new File("/sdcard/FakeCall/Audio")).mkdir();
        }
    }

    protected void onActivityResult(int i, int j, Intent intent) {

        super.onActivityResult(i, j, intent);
//        Uri uri=intent.getData();
        reqcode = i;
        if (j == -1) {
            //tomo foto
            if (i == 1) {
                File[] obj = (new File(Environment.getExternalStorageDirectory().toString())).listFiles();
                int k = obj.length;
                j = 0;
                boolean flag;
                while (j < k) {
                    if (!obj[j].getName().equals("temp.jpg"))
                        j++;
                    else {
                        camfile = obj[j];
                        break;
                    }
                }

                try {
                    if (camfile != null)
                        performCrop(Uri.fromFile(camfile));
                } catch (Exception exception) {
                }
            } else if (i == 2) {
                Uri data = intent.getData();
//                String[] as = new String[1];
//                as[0] = "_data";
//                Cursor cursor = getContentResolver().query(data, as, null, null, null);
//                cursor.moveToFirst();
//                picturePath = cursor.getString(cursor.getColumnIndex(as[0]));
//                cursor.close();
                performCrop(data);
            } else if (i == 3) {
                cropbitmap = (Bitmap) intent.getExtras().getParcelable("data");

                viewImage.setImageBitmap(cropbitmap);
                path = (new StringBuilder("/sdcard/FakeCallonex/Picture/")).append(String.valueOf(System.currentTimeMillis())).append(".jpg").toString();
                FileOutputStream obj = null;
                try {
                    obj = new FileOutputStream(new File(path));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                cropbitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 85, ((OutputStream) (obj)));
                try {
                    ((OutputStream) (obj)).flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    ((OutputStream) (obj)).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dbimgpath = path;
            } else if (i == 5) {
                Uri contact = intent.getData();
                Cursor cursor = getContentResolver().query(contact, null, null, null, null);
                if (cursor.moveToFirst()) {
                    String name = cursor.getString(cursor.getColumnIndex("display_name"));
                    String data = cursor.getString(cursor.getColumnIndex("data1"));
                    image_uri = cursor.getString(cursor.getColumnIndex("photo_uri"));
                    ename.setText(((CharSequence) (name)));
                    ephoneno.setText(data);
                    if (image_uri != null) {
                        viewImage.setImageURI(Uri.parse(image_uri));
                        dbimgpath = image_uri;
                    }
                    else{
                        viewImage.setImageResource(R.drawable.avatar);
                    }
                }

            }
            else if(i==888 && j== RESULT_OK){
                Uri ringtone= intent.getData();
                ringtonePath=ringtone.toString();
                ringtoneEt.setText(new File(getPath(ringtone)).getName());
            }
        }
    }

    public String getPath(Uri uri){
        if("content".equalsIgnoreCase(uri.getScheme())){
            String []projection= {"_data"};
            Cursor cursor=null;
            cursor= this.getContentResolver().query(uri,projection,null,null,null);
            int columnIndex= cursor.getColumnIndex("_data");
            if(cursor.moveToFirst())
                return cursor.getString(columnIndex);

        }
        else
            if("file".equalsIgnoreCase(uri.getScheme())){
                return uri.getPath();
            }
        return null;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                save();

                break;
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    @OnClick({R.id.add, R.id.linearLayout5})
    public void add(View view) {
        switch (view.getId()){
            case R.id.add:
                selectImage();
                break;
            case R.id.linearLayout5:
                Intent audio= new Intent(Intent.ACTION_GET_CONTENT);
                audio.setType("audio/*");
                audio.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(audio,888);
                break;
        }


    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        context = this;
        makeDirectory();
        //requestWindowFeature(1);
        setContentView(R.layout.customprofile);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Perfiles");
        setSupportActionBar(toolbar);
        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        // getWindow().addFlags(1024);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        parmpos = pref.getInt("pos1", 0);
        viewImage = (ImageView) findViewById(R.id.imgview);
        ename = (EditText) findViewById(R.id.et1);
        ephoneno = (EditText) findViewById(R.id.et2);
        b = (Button) findViewById(R.id.save);
        recb = (Button) findViewById(R.id.record);
        //vibrat = (Button) findViewById(R.id.vibrate);
        params = (Button) findViewById(R.id.params);
        spinner1 = (Spinner) findViewById(R.id.fspinner);
        vibcb = (CheckBox) findViewById(R.id.vibcbox);
//        reccb = (CheckBox) findViewById(R.id.reccbox);
        recordrl = (RelativeLayout) findViewById(R.id.recrl);
        vibraterl = (RelativeLayout) findViewById(R.id.vibrl);
        paramrl = (RelativeLayout) findViewById(R.id.parmrl);
        parambtn = (Button) findViewById(R.id.parmbt);
        pickcontact = (Button) findViewById(R.id.addcontact);
        mdb = new Mydatabase(getApplicationContext(), "Fakecallonex", null, 1);
        Bundle data = getIntent().getExtras();
        if (data != null && data.getBoolean("list", false)) {
            fromList = true;
            ename.setText(data.getString(Constants.name));
            ephoneno.setText(data.getString(Constants.phone));
            if (data.getString(Constants.vibrate).equals("y"))
                vibcb.setChecked(true);
            if (!data.getString(Constants.voice).equals("n")) {
                voicepath = data.getString(Constants.voice);
                //reccb.setChecked(true);
            }
            id = data.getString(Constants.id);
            viewImage.setImageURI(Uri.parse(data.getString(Constants.img)));
            dbimgpath = data.getString(Constants.img);

        } else
            fromList = false;

        final TextView message = (TextView) findViewById(R.id.message);
        dialogTime = (TextView) findViewById(R.id.time);
        //record.onCreate();
        recb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
//                    recordrl.setAnimation(AnimationUtils.makeOutAnimation(getApplicationContext(),true));
//                    recordL.setVisibility(View.INVISIBLE);
                    recording = false;
                    recb.setText(getString(R.string.record_text1));
                    customHandler.removeCallbacks(updateTimerThread2);
                    recb.setBackgroundResource(R.drawable.button);

                    //paro y salfo
                    record.stop();
                    voicepath = record.getRecordsrc();


                } else {
                    record = new Recordvoice();
                    dialogTime.setText("00:00");
                    startTime = SystemClock.uptimeMillis();
                    customHandler.postDelayed(updateTimerThread2, 0L);
                    recb.setText(getString(R.string.record_text2));
                    recb.setBackgroundResource(R.drawable.button_stop);
                                        //Profilecustom.reccb.setChecked(true);
                    recording = true;
                    //cambio icono y texto
                    record.startrec();
                }

            }
        });

        vibraterl.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                if (Profilecustom.vibcb.isChecked()) {
                    Profilecustom.vibcb.setChecked(false);
                    return;
                } else {
                    Profilecustom.vibcb.setChecked(true);
                    vbr = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                    vbr.vibrate(200L);
                    // return;
                }
            }
        });
        paramrl.setOnClickListener(new android.view.View.OnClickListener() {


            public void onClick(View view) {
//                view = new Intent(Profilecustom.this, Params.class);
                startActivity(new Intent(Profilecustom.this, Params.class));
            }
        });
//
        vibcb.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                if (Profilecustom.vibcb.isChecked()) {
                    vbr = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                    vbr.vibrate(200L);
                }
            }

        });
        params.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
//                view = new Intent(Profilecustom.this, Params);
                startActivity(new Intent(Profilecustom.this, Params.class));
            }

        });

        parambtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view) {
                startActivity(new Intent(Profilecustom.this, Params.class));
            }

        });
//
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    public void save() {
        Profilecustom.cname = Profilecustom.ename.getText().toString().trim();
        Profilecustom.cpno = Profilecustom.ephoneno.getText().toString().trim();
        int i = spinner1.getSelectedItemPosition();
        if (i == 0) {
            delaytime = 5000L;
        } else if (i == 1) {
            delaytime = 10000L;
        } else if (i == 2) {
            delaytime = 30000L;
        } else if (i == 3) {
            delaytime = 60000L;
        } else if (i == 4) {
            delaytime = 120000L;
        } else if (i == 5) {
            delaytime = 300000L;
        } else if (i == 6) {
            delaytime = 360000L;
        } else if (i == 7) {
            delaytime = 900000L;
        } else if (i == 8) {
            delaytime = 1800000L;
        } else {
            delaytime = 3600000L;
        }

        if (!fromList) {
            if(cname.isEmpty() || cpno.isEmpty())
            {
                Toast.makeText(this,"Debe insertar un nombre y numero de telefono",Toast.LENGTH_SHORT).show();
                return;
            }
            String id = System.currentTimeMillis() + "";
            if (Profilecustom.vibcb.isChecked() && !voicepath.isEmpty()) {
                mdb.insert(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), voicepath, "y", id,ringtonePath);
            } else if (!Profilecustom.vibcb.isChecked() && !voicepath.isEmpty()) {
                mdb.insert(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), voicepath, "n", id,ringtonePath);
            } else if (Profilecustom.vibcb.isChecked() && voicepath.isEmpty()) {
                mdb.insert(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), "n", "y", id,ringtonePath);
            } else {
                mdb.insert(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), "n", "n", id,ringtonePath);
            }
            String[] array = context.getResources().getStringArray(R.array.time_options);
            MyApplication.putProfile(this, cname, cpno, dbimgpath, array[spinner1.getSelectedItemPosition()], voicepath, "y", id, delaytime + "",ringtonePath);
        } else {
            if (Profilecustom.vibcb.isChecked() && !voicepath.isEmpty()) {
                mdb.updateCallrecord(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), voicepath, "y", id);
            } else if (!Profilecustom.vibcb.isChecked() && !voicepath.isEmpty()) {
                mdb.updateCallrecord(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), voicepath, "n", id);
            } else if (Profilecustom.vibcb.isChecked() && voicepath.isEmpty()) {
                mdb.updateCallrecord(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), "n", "y", id);
            } else {
                mdb.updateCallrecord(Profilecustom.cname, Profilecustom.cpno, dbimgpath, String.valueOf(spinner1.getSelectedItemPosition()), "n", "n", id);
            }
            String[] array = context.getResources().getStringArray(R.array.time_options);
            MyApplication.putProfile(this, cname, cpno, dbimgpath, array[spinner1.getSelectedItemPosition()], voicepath, "y", id, delaytime + "",ringtonePath);
        }
        finish();
        //MyApplication.setPosition(id);
//                mHander.postDelayed(mRunnable, delaytime);
//                Intent intent = new Intent("android.intent.action.MAIN");
//                intent.addCategory("android.intent.category.HOME");
//                intent.setFlags(0x10000000);
//                startActivity(intent);
//
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (player.isPlaying()) {
                player.stop();
            }
        } catch (Exception exception3) {
        }
        try {
            if (player != null) {
                player.reset();
                player.release();
                player = null;
            }
        } catch (Exception exception2) {
        }
        try {
            if (player1.isPlaying()) {
                player1.stop();
            }
        } catch (Exception exception1) {
        }
        try {
            if (player1 != null) {
                player1.reset();
                player1.release();
                player1 = null;
            }
            return;
        } catch (Exception exception) {
            return;
        }
    }

    public boolean onError(MediaPlayer mediaplayer, int i, int j) {
        if (player != null) {

            if (player.isPlaying()) {
                player.stop();
            }

            player.release();
            player = null;
        }
        return false;
    }


}
