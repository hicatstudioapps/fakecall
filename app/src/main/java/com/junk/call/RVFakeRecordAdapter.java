package com.junk.call;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.junk.call.other.Constants;

import java.io.File;
import java.util.List;

/**
 * Created by La Costra on 01/09/2015.
 */
public class RVFakeRecordAdapter extends RecyclerView.Adapter<RVFakeRecordAdapter.ViewHolder> {

    List<FakeRecord> items;
    DisplayImageOptions options;
    Mydatabase mdb;
    Context context;
    public RVFakeRecordAdapter(List<FakeRecord> items,Context context) {
        this.items = items;
        mdb = new Mydatabase(context, "Fakecallonex", null, 1);
        this.options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        this.context=context;
    }
    public void delteItem(int adapterPosition) {
        mdb.deleteoneRecord(items.get(adapterPosition).id);
    items.remove(adapterPosition);

        notifyItemRemoved(adapterPosition);
    }

    @Override
    public RVFakeRecordAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fake_record_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RVFakeRecordAdapter.ViewHolder holder, int position) {
        holder.cname.setText(items.get(position).cname);
        holder.cnum.setText(items.get(position).cnum);
        File image=new File(items.get(position).imagePath.toString());
        Uri img;
        if(!items.get(position).imagePath.toString().contains("content:"))
            img= Uri.fromFile(image);
        else
            img=items.get(position).imagePath;
//        ImageLoader.getInstance()
//                .displayImage("content://com.android.contacts/data/264", holder.cimage, options, new SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingStarted(String imageUri, View view) {
//
//                    }
//
//                    @Override
//                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    }
//
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    }
//                });
        if(!items.get(position).imagePath.toString().isEmpty())
        holder.cimage.setImageURI(items.get(position).imagePath);
        else
            holder.cimage.setImageResource(R.drawable.avatar);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView cname;
        TextView cnum;
        ImageView cimage;
        ImageView delete;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cname= (TextView)itemView.findViewById(R.id.cname);
            cnum= (TextView)itemView.findViewById(R.id.cnum);
            cimage= (ImageView) itemView.findViewById(R.id.imageView);
            delete=(ImageView)itemView.findViewById(R.id.imageView9);
           // delete= (ImageView) itemView.findViewById(R.id.imageView3);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
//                case R.id.imageView3:
//                    delteItem(getAdapterPosition());
//                    break;
                case R.id.imageView9:
                    delteItem(getAdapterPosition());
                    break;
                default:
                    Intent intent= new Intent(context,Profilecustom.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle= new Bundle();
                    //nombre
                    bundle.putBoolean("list", true);
                    String [] array= context.getResources().getStringArray(R.array.time_options);
                    MyApplication.putProfile(context, items.get(getAdapterPosition()).cname
                            , items.get(getAdapterPosition()).cnum,
                            items.get(getAdapterPosition()).imagePath.toString()
                            , array[Integer.parseInt(items.get(getAdapterPosition()).time)], items.get(getAdapterPosition()).rec, "y", items.get(getAdapterPosition()).id,items.get(getAdapterPosition()).delay,
                            items.get(getAdapterPosition()).ringtone);
                    ((Listviewimage)context).finish();
            }
        }
    }
}

