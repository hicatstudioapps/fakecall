package com.junk.call;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.junk.call.Recordvoice;

public class Clickrec extends Activity {
   AdView adView;
   Button rec;

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.clickrec);
      this.rec = (Button)this.findViewById(R.id.clickrec);

      try {
         this.adView = (AdView)this.findViewById(R.id.adView);
         AdRequest var3 = (new AdRequest.Builder()).build();
         this.adView.loadAd(var3);
      } catch (Exception var2) {
         ;
      }

      this.rec.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            Intent var2 = new Intent(Clickrec.this, Recordvoice.class);
            Clickrec.this.startActivity(var2);
         }
      });
   }
}
