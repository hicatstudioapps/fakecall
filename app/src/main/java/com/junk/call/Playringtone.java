package com.junk.call;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings.System;
import android.view.View;
import com.junk.call.Profilecustom;

public class Playringtone extends Activity {

   MediaPlayer player;
   Vibrator vbr;

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.main);
      if(Profilecustom.vibcb.isChecked()) {
         this.vbr = (Vibrator)this.getSystemService(VIBRATOR_SERVICE);
         this.vbr.vibrate(700L);
      }

      this.player = MediaPlayer.create(this, System.DEFAULT_RINGTONE_URI);
      this.player.start();
   }

   public void stopring(View var1) {
      try {
         this.player.stop();
         this.player.release();
         this.player = null;
      } catch (Exception var2) {
         ;
      }

   }
}
