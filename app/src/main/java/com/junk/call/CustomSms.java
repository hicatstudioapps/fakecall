package com.junk.call;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.junk.call.AddImage;
import com.junk.call.MainActivity;
import com.junk.call.ResAdapter;
import com.junk.call.other.Message;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("ResourceType")
public class CustomSms extends Activity {

   static int PICK_CONTACT_REQUEST = 5;
   static Bitmap bitmap;
   static Bitmap cropbitmap;
   static String image_uri;
   static EditText imsg;
   static List listMessages;
   static int listpos;
   static EditText msg1;
   static EditText msg2;
   static EditText msg3;
   static String msgname;
   static EditText name;
   static String nameVal;
   static EditText number;
   static int reqcode;
   static Bitmap thumbnail;
   int PIC_CROP;
   AdView adView;
   private ResAdapter adapter;
   Button add;
   File camfile;
   long delaytime;
   private InterstitialAd interstitial;
   Message m;
   private Handler mHander = new Handler();
   private final Runnable mRunnable = new Runnable() {
      public void run() {
         CustomSms.this.showNotification();

         try {
            CustomSms.this.mHander.postDelayed(CustomSms.this.mRunnable, CustomSms.this.delaytime);
         } catch (Exception var3) {
            ;
         }

         try {
            CustomSms.this.mHander.removeCallbacks(CustomSms.this.mRunnable);
         } catch (Exception var2) {
            ;
         }

      }
   };
   String msg;
   Button pickcontact;
   ListView reclist;
   Button save;
   Spinner spinner1;
   ImageView viewImage;

   private void appendMessage(final Message var1) {
      this.runOnUiThread(new Runnable() {
         public void run() {
            CustomSms.listMessages.add(var1);
            CustomSms.this.adapter.notifyDataSetChanged();
         }
      });
   }

   private static Bitmap createSquaredBitmap(Bitmap var0) {
      int var3 = Math.max(var0.getWidth(), var0.getHeight());
      Bitmap var1 = Bitmap.createBitmap(var3, var3, Config.ARGB_8888);
      Canvas var2 = new Canvas(var1);
      var2.drawColor(-1);
      var2.drawBitmap(var0, (float)((var3 - var0.getWidth()) / 2), (float)((var3 - var0.getHeight()) / 2), (Paint)null);
      return var1;
   }

   private void performCrop(Uri var1) {
      this.PIC_CROP = 3;

      try {
         Intent var2 = new Intent("com.android.camera.action.CROP");
         var2.setDataAndType(var1, "image/*");
         var2.putExtra("crop", "true");
         var2.putExtra("aspectX", 1);
         var2.putExtra("aspectY", 1);
         var2.putExtra("outputX", 128);
         var2.putExtra("outputY", 128);
         var2.putExtra("return-data", true);
         this.startActivityForResult(var2, this.PIC_CROP);
      } catch (ActivityNotFoundException var3) {
         ;
      }

   }

   private void selectImage() {
      final CharSequence[] var1 = new CharSequence[]{"Take Photo", "Choose from Gallery", "Cancel"};
      Builder var2 = new Builder(this);
      var2.setTitle("Add Photo!");
      var2.setItems(var1, new OnClickListener() {
         public void onClick(DialogInterface var1x, int var2) {
            Intent var3;
            if(var1[var2].equals("Take Photo")) {
               var3 = new Intent("android.media.action.IMAGE_CAPTURE");
               CustomSms.this.camfile = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
               var3.putExtra("output", Uri.fromFile(CustomSms.this.camfile));
               CustomSms.this.startActivityForResult(var3, 1);
            } else if(var1[var2].equals("Choose from Gallery")) {
               var3 = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
               CustomSms.this.startActivityForResult(var3, 2);
            } else if(var1[var2].equals("Cancel")) {
               var1x.dismiss();
            }

         }
      });
      var2.show();
   }

   public void Showdialog() {
      View var2 = LayoutInflater.from(this).inflate(R.layout.dialog, (ViewGroup)null);
      Builder var1 = new Builder(this);
      var1.setView(var2);
      final EditText var3 = (EditText)var2.findViewById(R.id.editTextDialogUserInput);
      var3.setText(this.msg);
      var1.setCancelable(false).setPositiveButton("OK", new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            CustomSms.this.msg = var3.getText().toString().trim();
            CustomSms.this.m = new Message(CustomSms.nameVal, CustomSms.this.msg, (new SimpleDateFormat("hh:mm a")).format(new Date()), false);
            CustomSms.listMessages.remove(CustomSms.listpos);
            CustomSms.listMessages.add(CustomSms.listpos, CustomSms.this.m);
            CustomSms.this.adapter.notifyDataSetChanged();
         }
      }).setNegativeButton("Cancel", new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.cancel();
         }
      });
      var1.create().show();
   }

   public void cancelNotification(int var1) {
      if("notification" != null) {
          //noinspection ResourceType
          ((NotificationManager)this.getApplicationContext().getSystemService("notification")).cancel(var1);
      }

   }

   public void displayInterstitial() {
      if(this.interstitial.isLoaded()) {
         this.interstitial.show();
      }

   }

   protected void onActivityResult(int param1, int param2, Intent param3) {
      // $FF: Couldn't be decompiled
   }

   @SuppressWarnings("ResourceType")
   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.requestWindowFeature(1);
      this.setContentView(R.layout.smsmain);
      this.save = (Button)this.findViewById(R.id.save);
      this.add = (Button)this.findViewById(R.id.add);
      this.reclist = (ListView)this.findViewById(R.id.smslist);
      imsg = (EditText)this.findViewById(R.id.msg);
      name = (EditText)this.findViewById(R.id.et1);
      number = (EditText)this.findViewById(R.id.et2);
      this.viewImage = (ImageView)this.findViewById(R.id.imgview);
      this.pickcontact = (Button)this.findViewById(R.id.addcontact);
          this.spinner1 = (Spinner)this.findViewById(R.id.fspinner);
      listMessages = new ArrayList();
      this.adapter = new ResAdapter(this.getApplicationContext(), listMessages);
      this.reclist.setAdapter(this.adapter);

      AdRequest var5;
      try {
         this.adView = (AdView)this.findViewById(R.id.adView);
         var5 = (new AdRequest.Builder()).build();
         this.adView.loadAd(var5);
      } catch (Exception var4) {
         ;
      }

      try {
         View var6 = this.findViewById(R.id.top);
         if(!AddImage.isInternetPresent.booleanValue()) {
            var6.setVisibility(8);
         }
      } catch (Exception var3) {
         ;
      }

      try {
         this.interstitial = new InterstitialAd(this);
         this.interstitial.setAdUnitId("ca-app-pub-8098707670633703/5411584071");
         var5 = (new AdRequest.Builder()).build();
         this.interstitial.loadAd(var5);
      } catch (Exception var2) {
         ;
      }

      this.interstitial.setAdListener(new AdListener() {
         public void onAdLoaded() {
            CustomSms.this.displayInterstitial();
         }
      });
      this.pickcontact.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            Intent var2 = new Intent("android.intent.action.PICK", Contacts.CONTENT_URI);
            var2.setType("vnd.android.cursor.dir/phone_v2");
            CustomSms.this.startActivityForResult(var2, 5);
         }
      });
      this.viewImage.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            CustomSms.this.selectImage();
         }
      });
      this.reclist.setOnItemClickListener(new OnItemClickListener() {
         public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
            CustomSms.listpos = var3;
            Message var6 = (Message)CustomSms.listMessages.get(var3);
            CustomSms.this.msg = var6.getMessage();
            CustomSms.nameVal = var6.getFromName();
            CustomSms.this.options();
         }
      });
      this.add.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            if(CustomSms.imsg.getText().toString().trim().equals("")) {
               CustomSms.imsg.setError("Enter  msg");
            } else if(CustomSms.name.getText().toString().trim().equals("")) {
               CustomSms.name.setError("Enter  name");
            } else if(CustomSms.number.getText().toString().trim().equals("")) {
               CustomSms.number.setError("Enter phone number");
            } else {
               CustomSms.this.m = new Message(CustomSms.name.getText().toString().trim(), CustomSms.imsg.getText().toString().trim(), (new SimpleDateFormat("hh:mm a")).format(new Date()), false);
               CustomSms.this.appendMessage(CustomSms.this.m);
               CustomSms.imsg.setText((CharSequence)null);
            }

         }
      });
      this.save.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            CustomSms.msgname = CustomSms.name.getText().toString().trim();
            if(CustomSms.name.getText().toString().trim().equals("")) {
               CustomSms.name.setError("Enter  name");
            } else if(CustomSms.number.getText().toString().trim().equals("")) {
               CustomSms.number.setError("Enter phone number");
            } else {
               int var2 = CustomSms.this.spinner1.getSelectedItemPosition();
               if(var2 == 0) {
                  CustomSms.this.delaytime = 5000L;
               } else if(var2 == 1) {
                  CustomSms.this.delaytime = 10000L;
               } else if(var2 == 2) {
                  CustomSms.this.delaytime = 30000L;
               } else if(var2 == 3) {
                  CustomSms.this.delaytime = 60000L;
               } else if(var2 == 4) {
                  CustomSms.this.delaytime = 120000L;
               } else if(var2 == 5) {
                  CustomSms.this.delaytime = 300000L;
               } else if(var2 == 6) {
                  CustomSms.this.delaytime = 360000L;
               } else if(var2 == 7) {
                  CustomSms.this.delaytime = 900000L;
               } else if(var2 == 8) {
                  CustomSms.this.delaytime = 1800000L;
               } else {
                  CustomSms.this.delaytime = 3600000L;
               }

               CustomSms.this.mHander.postDelayed(CustomSms.this.mRunnable, CustomSms.this.delaytime);
               CustomSms.this.finish();
            }

         }
      });
   }

   public void options() {
      final CharSequence[] var2 = new CharSequence[]{"Remove", "Edit Msg", "Cancel"};
      Builder var1 = new Builder(this);
      var1.setTitle("Modify Msg");
      var1.setItems(var2, new OnClickListener() {
         public void onClick(DialogInterface var1, int var2x) {
            if(var2[var2x].equals("Remove")) {
               CustomSms.listMessages.remove(CustomSms.listpos);
               CustomSms.this.adapter.notifyDataSetChanged();
            } else if(var2[var2x].equals("Edit Msg")) {
               CustomSms.this.Showdialog();
            } else if(var2[var2x].equals("Cancel")) {
               var1.dismiss();
            }

         }
      });
      var1.show();
   }

   public void showNotification() {
      Uri var2 = RingtoneManager.getDefaultUri(2);
      PendingIntent var1 = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 1207959552);
      NotificationCompat.Builder var3 = (new NotificationCompat.Builder(this)).setSmallIcon(R.drawable.msgnot).setContentTitle(name.getText().toString().trim()).setContentText("New Message for you.").setSound(var2);
      var3.setContentIntent(var1);
      ((NotificationManager)this.getSystemService(NOTIFICATION_SERVICE)).notify(0, var3.build());
   }
}
