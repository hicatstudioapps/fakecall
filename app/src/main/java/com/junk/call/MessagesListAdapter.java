package com.junk.call;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.junk.call.other.Message;
import java.util.List;

public class MessagesListAdapter extends BaseAdapter {
   private Context context;
   private List messagesItems;

   public MessagesListAdapter(Context var1, List var2) {
      this.context = var1;
      this.messagesItems = var2;
   }

   public int getCount() {
      return this.messagesItems.size();
   }

   public Object getItem(int var1) {
      return this.messagesItems.get(var1);
   }

   public long getItemId(int var1) {
      return (long)var1;
   }

   @SuppressLint({"InflateParams"})
   public View getView(int var1, View var2, ViewGroup var3) {
      Message var8 = (Message)this.messagesItems.get(var1);
      LayoutInflater var7 = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      if(((Message)this.messagesItems.get(var1)).isSelf()) {
         var2 = var7.inflate(R.layout.list_item_message_right, (ViewGroup)null);
      } else {
         var2 = var7.inflate(R.layout.list_item_message_left, (ViewGroup)null);
      }

      TextView var6 = (TextView)var2.findViewById(R.id.lblMsgFrom);
      TextView var5 = (TextView)var2.findViewById(R.id.txtMsg);
      TextView var4 = (TextView)var2.findViewById(R.id.timeset);
      var5.setText(var8.getMessage());
      var6.setText(var8.getFromName());
      var4.setText(var8.getTime());
      return var2;
   }
}
