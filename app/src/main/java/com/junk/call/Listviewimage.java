package com.junk.call;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.example.lacostra.gbar.ChromeFloatingCirclesDrawable;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

@SuppressWarnings("ResourceType")
public class Listviewimage extends AppCompatActivity {

    public static int position;
   AdView adView;
   Mydatabase db;
   RecyclerView recyclerView;
   LoadFakeRecords loadFakeRecords;

    @InjectView(R.id.progressBar)
    ProgressBar loading;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.add)
    View add;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }

    public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.gridview);
       ButterKnife.inject(this);
       recyclerView=(RecyclerView)findViewById(R.id.rv);
       toolbar.setTitle(getString(R.string.save_call_title));
//       toolbar.setNavigationIcon(R.drawable.ic_action_back);
       setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setHomeButtonEnabled(true);
      LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this);
      recyclerView.setLayoutManager(linearLayoutManager);
        AdRequest var13;
        try {
            this.adView = (AdView)this.findViewById(R.id.adView);
            var13 = (new AdRequest.Builder()).build();
            this.adView.loadAd(var13);
            this.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception var8) {
            ;
        }

       Rect bounds = loading.getIndeterminateDrawable().getBounds();
       loading.setIndeterminateDrawable(getProgressDrawable());
       loading.getIndeterminateDrawable().setBounds(bounds);
       add.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent(getApplication(), Profilecustom.class));
           }
       });
   }

    private Drawable getProgressDrawable() {
        int[] colors = new int[4];
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        colors[0] = getResources().getColor(R.color.red);
        colors[1] = getResources().getColor(R.color.blue);
        colors[2] = getResources().getColor(R.color.yellow);
        colors[3] = getResources().getColor(R.color.green);
        return  new ChromeFloatingCirclesDrawable.Builder(this)
                .colors(colors)
                .build();

    }

    public void onCreateContextMenu(ContextMenu var1, View var2, ContextMenuInfo var3) {
      this.getMenuInflater();
      super.onCreateContextMenu(var1, var2, var3);
   }

   public void onResume() {
      super.onResume();
       recyclerView.setVisibility(View.GONE);
       this.db = new Mydatabase(this.getApplicationContext(), "Fakecallonex", (CursorFactory)null, 1);
       loadFakeRecords= new LoadFakeRecords(this);
       loadFakeRecords.execute(this.db.select());
     // this.updateList();
   }

    @Override
    protected void onStop() {
        super.onStop();
        loading.setVisibility(View.VISIBLE);
        add.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }
    //


    class LoadFakeRecords extends AsyncTask<Cursor,Void,List<FakeRecord>>{

        Context context;

        public LoadFakeRecords(Context context) {
            this.context = context;
        }

        @Override
        protected List<FakeRecord> doInBackground(Cursor... params) {
            List<FakeRecord> result= new LinkedList<>();
            Cursor cr=params[0];
            if(cr.moveToFirst()){

            }
            for(cr.moveToFirst();!cr.isAfterLast();cr.moveToNext()){
                int i=Integer.parseInt(cr.getString(4));
                long delaytime;
                if (i == 0) {
                    delaytime = 5000L;
                } else if (i == 1) {
                    delaytime = 10000L;
                } else if (i == 2) {
                    delaytime = 30000L;
                } else if (i == 3) {
                    delaytime = 60000L;
                } else if (i == 4) {
                    delaytime = 120000L;
                } else if (i == 5) {
                    delaytime = 300000L;
                } else if (i == 6) {
                    delaytime = 360000L;
                } else if (i == 7) {
                    delaytime = 900000L;
                } else if (i == 8) {
                    delaytime = 1800000L;
                } else {
                    delaytime = 3600000L;
                }
            result.add(new FakeRecord(Uri.parse(cr.getString(3)),cr.getString(1),cr.getString(2),cr.getString(6),cr.getString(5),cr.getString(0),cr.getString(4),delaytime+"", cr.getString(7)));
                       }
               return result;
        }

        @Override
        protected void onPostExecute(List<FakeRecord> fakeRecords) {
            recyclerView.setAdapter(new RVFakeRecordAdapter(fakeRecords, context));
            add.setAnimation(AnimationUtils.loadAnimation(context, R.anim.abc_fade_in));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loading.setVisibility(View.GONE);
                    recyclerView.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
                    recyclerView.setVisibility(View.VISIBLE);
                    add.setVisibility(View.VISIBLE);
                }
            },3000)      ;
        }
    }
}
