package com.junk.call.other;

public class Message {
   private String fromName;
   private boolean isSelf;
   private String message;
   private String time;

   public Message() {
   }

   public Message(String var1, String var2, String var3, boolean var4) {
      this.fromName = var1;
      this.message = var2;
      this.time = var3;
      this.isSelf = var4;
   }

   public String getFromName() {
      return this.fromName;
   }

   public String getMessage() {
      return this.message;
   }

   public String getTime() {
      return this.time;
   }

   public boolean isSelf() {
      return this.isSelf;
   }

   public void setFromName(String var1) {
      this.fromName = var1;
   }

   public void setMessage(String var1) {
      this.message = var1;
   }

   public void setSelf(boolean var1) {
      this.isSelf = var1;
   }

   public void setTime(String var1) {
      this.time = var1;
   }
}
