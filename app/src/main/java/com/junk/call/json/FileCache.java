package com.junk.call.json;

import android.content.Context;
import android.os.Environment;
import java.io.File;

public class FileCache {
   private File cacheDir;

   public FileCache(Context var1) {
      if(Environment.getExternalStorageState().equals("mounted")) {
         this.cacheDir = new File(Environment.getExternalStorageDirectory(), "JsonParseTutorialCache");
      } else {
         this.cacheDir = var1.getCacheDir();
      }

      if(!this.cacheDir.exists()) {
         this.cacheDir.mkdirs();
      }

   }

   public void clear() {
      File[] var1 = this.cacheDir.listFiles();
      if(var1 != null) {
         int var3 = var1.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            var1[var2].delete();
         }
      }

   }

   public File getFile(String var1) {
      int var2 = var1.hashCode();
      return new File(this.cacheDir, String.valueOf(var2));
   }
}
