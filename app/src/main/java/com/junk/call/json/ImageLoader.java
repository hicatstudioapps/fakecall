package com.junk.call.json;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;
import com.junk.call.json.FileCache;
import com.junk.call.json.MemoryCache;
import com.junk.call.json.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
   ExecutorService executorService;
   FileCache fileCache;
   Handler handler = new Handler();
   private Map imageViews = Collections.synchronizedMap(new WeakHashMap());
   MemoryCache memoryCache = new MemoryCache();
   final int stub_id = 2130837616;

   public ImageLoader(Context var1) {
      this.fileCache = new FileCache(var1);
      this.executorService = Executors.newFixedThreadPool(5);
   }

   private Bitmap decodeFile(File param1) {
      // $FF: Couldn't be decompiled
      return null;
   }

   private Bitmap getBitmap(String var1) {
      File var3 = this.fileCache.getFile(var1);
      Bitmap var2 = this.decodeFile(var3);
      Bitmap var6;
      if(var2 != null) {
         var6 = var2;
      } else {
         try {
            HttpURLConnection var7 = (HttpURLConnection)(new URL(var1)).openConnection();
            var7.setConnectTimeout(30000);
            var7.setReadTimeout(30000);
            var7.setInstanceFollowRedirects(true);
            InputStream var4 = var7.getInputStream();
            FileOutputStream var8 = new FileOutputStream(var3);
            Utils.CopyStream(var4, var8);
            var8.close();
            var7.disconnect();
            var6 = this.decodeFile(var3);
         } catch (Throwable var5) {
            var5.printStackTrace();
            if(var5 instanceof OutOfMemoryError) {
               this.memoryCache.clear();
            }

            var6 = null;
         }
      }

      return var6;
   }

   private void queuePhoto(String var1, ImageView var2) {
      ImageLoader.PhotoToLoad var3 = new ImageLoader.PhotoToLoad(var1, var2);
      this.executorService.submit(new ImageLoader.PhotosLoader(var3));
   }

   public void DisplayImage(String var1, ImageView var2) {
      this.imageViews.put(var2, var1);
      Bitmap var3 = this.memoryCache.get(var1);
      if(var3 != null) {
         var2.setImageResource(17170445);
         var2.setImageBitmap(var3);
      } else {
         this.queuePhoto(var1, var2);
      }

   }

   public void clearCache() {
      this.memoryCache.clear();
      this.fileCache.clear();
   }

   boolean imageViewReused(ImageLoader.PhotoToLoad var1) {
      String var2 = (String)this.imageViews.get(var1.imageView);
      boolean var3;
      if(var2 != null && var2.equals(var1.url)) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   class BitmapDisplayer implements Runnable {
      Bitmap bitmap;
      ImageLoader.PhotoToLoad photoToLoad;

      public BitmapDisplayer(Bitmap var2, ImageLoader.PhotoToLoad var3) {
         this.bitmap = var2;
         this.photoToLoad = var3;
      }

      public void run() {
         if(!ImageLoader.this.imageViewReused(this.photoToLoad) && this.bitmap != null) {
            this.photoToLoad.imageView.setImageBitmap(this.bitmap);
         }

      }
   }

   private class PhotoToLoad {
      public ImageView imageView;
      public String url;

      public PhotoToLoad(String var2, ImageView var3) {
         this.url = var2;
         this.imageView = var3;
      }
   }

   class PhotosLoader implements Runnable {
      ImageLoader.PhotoToLoad photoToLoad;

      PhotosLoader(ImageLoader.PhotoToLoad var2) {
         this.photoToLoad = var2;
      }

      public void run() {
         try {
            if(!ImageLoader.this.imageViewReused(this.photoToLoad)) {
               Bitmap var1 = ImageLoader.this.getBitmap(this.photoToLoad.url);
               ImageLoader.this.memoryCache.put(this.photoToLoad.url, var1);
               if(!ImageLoader.this.imageViewReused(this.photoToLoad)) {
                  ImageLoader.BitmapDisplayer var3 = ImageLoader.this.new BitmapDisplayer(var1, this.photoToLoad);
                  ImageLoader.this.handler.post(var3);
               }
            }
         } catch (Throwable var2) {
            var2.printStackTrace();
         }

      }
   }
}
