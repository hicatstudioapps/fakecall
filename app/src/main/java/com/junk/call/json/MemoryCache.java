package com.junk.call.json;

import android.graphics.Bitmap;
import android.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MemoryCache {
   private static final String TAG = "MemoryCache";
   private Map cache = Collections.synchronizedMap(new LinkedHashMap(10, 1.5F, true));
   private long limit = 1000000L;
   private long size = 0L;

   public MemoryCache() {
      this.setLimit(Runtime.getRuntime().maxMemory() / 4L);
   }

   private void checkSize() {
      Log.i("MemoryCache", "cache size=" + this.size + " length=" + this.cache.size());
      if(this.size > this.limit) {
         Iterator var1 = this.cache.entrySet().iterator();

         while(var1.hasNext()) {
            Entry var2 = (Entry)var1.next();
            this.size -= this.getSizeInBytes((Bitmap)var2.getValue());
            var1.remove();
            if(this.size <= this.limit) {
               break;
            }
         }

         Log.i("MemoryCache", "Clean cache. New size " + this.cache.size());
      }

   }

   public void clear() {
      try {
         this.cache.clear();
         this.size = 0L;
      } catch (NullPointerException var2) {
         var2.printStackTrace();
      }

   }

   public Bitmap get(String var1) {
      Bitmap var3;
      try {
         if(this.cache.containsKey(var1)) {
            var3 = (Bitmap)this.cache.get(var1);
            return var3;
         }
      } catch (NullPointerException var2) {
         var2.printStackTrace();
         var3 = null;
         return var3;
      }

      var3 = null;
      return var3;
   }

   long getSizeInBytes(Bitmap var1) {
      long var2;
      if(var1 == null) {
         var2 = 0L;
      } else {
         var2 = (long)(var1.getRowBytes() * var1.getHeight());
      }

      return var2;
   }

   public void put(String var1, Bitmap var2) {
      try {
         if(this.cache.containsKey(var1)) {
            this.size -= this.getSizeInBytes((Bitmap)this.cache.get(var1));
         }

         this.cache.put(var1, var2);
         this.size += this.getSizeInBytes(var2);
         this.checkSize();
      } catch (Throwable var3) {
         var3.printStackTrace();
      }

   }

   public void setLimit(long var1) {
      this.limit = var1;
      Log.i("MemoryCache", "MemoryCache will use up to " + (double)this.limit / 1024.0D / 1024.0D + "MB");
   }
}
